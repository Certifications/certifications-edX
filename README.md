# edX-certificates

List and Details
- Course "Introduction to Linux" (LFS101x.2)  
  by The Linux Foundation on edX.  
  Honor Code Certificate earned on July 15th, 2015. Grade 100%
- Course "Matrix Algebra and Linear Models" (PH525.2x)  
  by Harvard University on edX.  
  Honor Code Certificate earned on July 23rd, 2015. Grade 96%
- Course "Introduction to R" (DAT204x)  
  by Microsoft on edX.  
  Honor Code Certificate earned on September 4th, 2015. Grade 100%
- Course "Introduction to Computer Science and Programming Using Python" (MIT6.00.1x)  
  by Massachusetts Institute of Technology on edX.  
  Honor Code Certificate earned on November 6th, 2015. Grade 99%
- Course "Introduction to Computational Thinking and Data Science" (MIT6.00.2x)  
  by Massachusetts Institute of Technology on edX.  
  Honor Code Certificate earned on December 29th, 2015. Grade 93%